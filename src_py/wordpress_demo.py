import MySQLdb
import datetime, xmlrpclib
import getpass

db = MySQLdb.connect("localhost", "root", "root", "mytest")
cursor = db.cursor()
print ""
print "The wordpress db in mysql is: "
print ""
posts = []
d = datetime.datetime.strptime("2017-08-20 02:19", "%Y-%m-%d %H:%M")
print ""
print 'The posts will be published on ', d
print ""
date_created = xmlrpclib.DateTime(d)
categories = ["Some category"]
tags = ["tag1", "tag2"]
with db: 
    cursor.execute("SELECT * FROM blogposts")
    rows = cursor.fetchall()
    for row in rows:
	data = {'title': str(row[0]) + '.' + row[1],
        	'description': row[2],
        	'dateCreated': date_created,
        	'categories': categories,
        	'mt_keywords': tags}
	posts.append(data)
        print row[0], 'Title ', row[1], ' ', 'Description', row[2]
db.close()
print "Starting parsing MYSQL database"

### Input wordpress login and password, uncomment to type login too
#login = raw_input("Enter wordpress login:")
#print(login)
wp_password = getpass.getpass("Type wordpress page password: ")
# someurl must be the same as in yours wordpress domain
wp_url = "https://someurl.wordpress.com/xmlrpc.php"
wp_username = "ofeweb"

wp_blogid = ""
status_draft = 0
status_published = 1
server = xmlrpclib.ServerProxy(wp_url)

for i, p in enumerate(posts):
  print ""
  print "Sending post for ", i, ' - ', p['title']
  post_id = server.metaWeblog.newPost(wp_blogid, wp_username, wp_password, p, status_published)

wp_password = ""
